Version 1.8.1
===============
- Added intents for requesting top activities and meta stats from Charlemagne.
- Performance improvements around filtering items from a list.
- Added the Sentry.io library for reporting unexpected errors so alerts can be provided.
- Improved documentation to silence Go linter warnings
- Fixed a bug causing users to not be able to equip a named loadout.

